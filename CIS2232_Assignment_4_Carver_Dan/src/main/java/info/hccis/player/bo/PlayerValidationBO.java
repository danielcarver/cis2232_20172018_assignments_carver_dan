package info.hccis.player.bo;

import info.hccis.player.model.jpa.Player;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @since Oct 18, 2017
 * @author bjmaclean
 */
public class PlayerValidationBO {

    public static ArrayList<String> validatePlayer(Player player) {
        
        
        
        //Do some validation before trying the update.
        ArrayList<String> messages = new ArrayList();
        
        if (player.getAmountPaid() < 0)
        {
            messages.add("Must pay at least $0");
        }
        
        if (player.getAmountPaid() > 50)
        {
            messages.add("Cannot pay over $50");
        }
        
        /*//First name required
        if (player.getFirstName().isEmpty()) {
            messages.add("First name is required");
        }
        //Last name required
        if (player.getLastName().isEmpty()) {
            messages.add("Last name is required");
        }

        //dob is in yyyy/mm/dd
        boolean dobError = false;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setLenient(false);
            formatter.parse(player.getDob());
        } catch (Exception e) {
            System.out.println("BJM - error parsing date");
            dobError = true;
            messages.add("Date must be in yyyy-MM-dd format");
        }

        if (!dobError) {
            //Validate that the player is born within last 16 years.
            //Get current year
            //From Stack Overflow
            Calendar now = Calendar.getInstance();   // Gets the current date and time
            int year = now.get(Calendar.YEAR);       // The current year

            //Get their birth year
            int birthYear = Integer.parseInt(player.getDob().substring(0, 4));

            //Subtract
            if (year - birthYear > 16) {
                messages.add("Player must be born after " + (year - 17));
            }
        }*/
        return messages;
    }

}
