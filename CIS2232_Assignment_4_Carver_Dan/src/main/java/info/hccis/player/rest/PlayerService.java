package info.hccis.player.rest;

import com.google.gson.Gson;
import info.hccis.player.dao.PlayerDAO;
//import info.hccis.player.data.springdatajpa.PlayerRepository;
import info.hccis.player.model.Player;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author dan carver
 */

@Path("/PlayerService")
public class PlayerService {

    /**
     * This is a sample web service operation
     */
    
    //private final PlayerRepository pr;
    
//    @Autowired
//    public PlayerService(PlayerRepository pr)
//    {
//        this.pr = pr;
//    }
    
    @GET
    @Path("/players")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlayers() {

        //Get the playerss from the database
        ArrayList<Player> players = PlayerDAO.selectAll();
        Gson gson = new Gson();

        int statusCode = 200;
        if(players.isEmpty()){
            statusCode = 204;
        }
        
        String temp = "";
        temp = gson.toJson(players);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
}
