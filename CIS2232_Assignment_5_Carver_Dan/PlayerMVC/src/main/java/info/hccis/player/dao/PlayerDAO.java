package info.hccis.player.dao;

import java.sql.Connection;
import info.hccis.player.dao.util.*;
import info.hccis.player.model.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with Campers
 *
 * @author dan carver
 * @since Oct. 9th, 2017
 */
public class PlayerDAO 
{

    private final static Logger LOGGER = Logger.getLogger(PlayerDAO.class.getName());

    /**
     * Select a specific player
     *
     * @author dan carver
     * @since Oct. 9th, 2017
     * @return
     */
    
    public static synchronized void delete(String name) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtil.getConnection();

            sql = "delete FROM player where name = ? limit 1";

            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtil.close(ps, conn);
        }
    }
    
    public static Player select(String name) 
    {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Player thePlayer = null;
        try 
        {
            conn = ConnectionUtil.getConnection();

            sql = "SELECT * FROM player where name = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) 
            {
                name = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phone = rs.getString("phoneNumber");
                String email = rs.getString("emailAddress");
                int amountPaid = rs.getInt("amountPaid");
                
                thePlayer = new Player(name, parentName, phone, email, amountPaid);
            }
        } 
        catch (Exception e) 
        {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } 
        finally 
        {
            DbUtil.close(ps, conn);
        }
        return thePlayer;
    }

    /**
     * Select all players
     *
     * @author dan carver
     * @since Oct. 9th, 2017
     * @return
     */
    public static ArrayList<Player> selectAll() 
    {

        ArrayList<Player> player = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try 
        {
            conn = ConnectionUtil.getConnection();

            sql = "SELECT * FROM player";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) 
            {
                String name = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phone = rs.getString("phoneNumber");
                String email = rs.getString("emailAddress");
                int amountPaid = rs.getInt("amountPaid");
                
                player.add(new Player(name, parentName, phone, email, amountPaid));
            }
            
        }
        catch (Exception e) 
        {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } 
        finally 
        {
            DbUtil.close(ps, conn);
        }
        return player;
    }
    
    /**
     * Insert a player into the db
     *
     * @author dan carver
     * @since Oct. 9th, 2017
     * @return
     */
    public static synchronized Player insert(Player player) throws Exception 
    {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtil.getConnection();
            
                sql = "INSERT INTO `player`(`name`, `parentName`, `phoneNumber`, `emailAddress`, `amountPaid`)"
                        + "VALUES (?,?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setString(1, player.getName());
                ps.setString(2, player.getParentName());
                ps.setString(3, player.getPhone());
                ps.setString(4, player.getEmail());
                ps.setInt(5, player.getAmountPaid());

            
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } 
        catch (Exception e)
        {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } 
        finally 
        {
            DbUtil.close(ps, conn);
        }
        
        return player;
    }

    /**
     * update a specific player in the db
     *
     * @author dan carver
     * @since Oct. 9th, 2017
     * @return
     */
    public static synchronized Player update(Player player) throws Exception 
    {
//        System.out.println("inserting camper");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        /*
         * Setup the sql to update or insert the row.
         */
        try 
        {
            conn = ConnectionUtil.getConnection();
            
            sql = "UPDATE `player` SET `amountPaid`=? WHERE name =?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, player.getAmountPaid());
            ps.setString(2, player.getName());
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();
            

        } 
        catch (Exception e) 
        {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } 
        finally 
        {
            DbUtil.close(ps, conn);
        }
        return player;
    }
    
    /**
     * get total of all amounts paid
     *
     * @author dan carver
     * @since Oct. 9th, 2017
     * @return
     */
    public static void total()
    {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        int total = 0;
        
        try
        {
            conn = ConnectionUtil.getConnection();
            
            sql = "SELECT SUM(amountPaid) FROM `player`";
            
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next())
            {
                total = rs.getInt(1);
            }
            
            System.out.println("The total amount paid by all players is $" + total);
        }
        catch (Exception e)
        {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        finally
        {
            DbUtil.close(ps, conn);
        }
    }

}

//    /**
//     * This method will insert.
//     *
//     * @return
//     * @author BJ
//     * @since 20140615
//     */
//    public static void insertNotification(Notification notification) throws Exception {
//        System.out.println("inserting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "INSERT INTO `notification`(`notification_type_code`,  "
//                    + "`notification_detail`, `status_code`, `created_date_time`, "
//                    + "`created_user_id`, `updated_date_time`, `updated_user_id`) "
//                    + "VALUES (?, ?, 1, sysdate(), ?, sysdate(), ?)";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notification.getNotificationType());
//            ps.setString(2, notification.getNotificationDetail());
//            ps.setString(3, notification.getUserId());
//            ps.setString(4, notification.getUserId());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//
//    /**
//     * Delete the specified member education (set to inactive)
//     * @param memberId
//     * @param memberEducationSequence 
//     */
//    public static void deleteNotification(int notificationId) throws Exception{
//        
//        System.out.println("deleting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "update notification set status_code = 0, updated_date_time = sysdate() "
//                + "where notification_id = ? ";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notificationId);
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//        
//
//    
//    public static ArrayList<Notification> getNotifications() {
//        ArrayList<Notification> notifications = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM notification WHERE status_code = 1 order by created_date_time desc";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Notification newNotification = new Notification();
//                newNotification.setNotificationId(rs.getInt("notification_id"));
//                newNotification.setNotificationDetail(rs.getString("notification_detail"));
//                newNotification.setNotificationType(rs.getInt("notification_type_code"));
//                newNotification.setNotificationDate(rs.getString("created_date_time"));
//                newNotification.setUserId(rs.getString("created_user_id"));
//                notifications.add(newNotification);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return notifications;
//    }

