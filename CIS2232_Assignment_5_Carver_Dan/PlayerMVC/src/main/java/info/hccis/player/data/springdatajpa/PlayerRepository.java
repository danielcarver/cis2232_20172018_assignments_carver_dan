package info.hccis.player.data.springdatajpa;

import info.hccis.player.model.jpa.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends CrudRepository<Player, String> {


}