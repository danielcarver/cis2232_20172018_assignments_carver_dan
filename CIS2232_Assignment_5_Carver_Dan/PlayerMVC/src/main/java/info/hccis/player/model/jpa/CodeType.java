package info.hccis.player.model.jpa;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Purpose: Java class linked to the database table.
 *
 * @author BJ
 * @since 20151002
 */
@Entity
@Table(name = "CodeType")
public class CodeType {

    private static final long serialVersionUID = 1L;
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CodeTypeId")
    private Integer codeTypeId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "englishDescription")
    private String englishDescription;
    
    @Size(max = 100)
    @Column(name = "frenchDescription")
    private String frenchDescription;
    
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;

    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;

    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;

    public CodeType() {
    }

    public CodeType(Integer codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    public CodeType(Integer codeTypeId, String englishDescription) {
        this.codeTypeId = codeTypeId;
        this.englishDescription = englishDescription;
    }

    public Integer getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(Integer codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String frenchDescription) {
        this.frenchDescription = frenchDescription;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeTypeId != null ? codeTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof info.hccis.player.model.jpa.CodeType)) {
            return false;
        }
        info.hccis.player.model.jpa.CodeType other = (info.hccis.player.model.jpa.CodeType) object;
        if ((this.codeTypeId == null && other.codeTypeId != null) || (this.codeTypeId != null && !this.codeTypeId.equals(other.codeTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.CodeType[ codeTypeId=" + codeTypeId + " ]";
    }

}
