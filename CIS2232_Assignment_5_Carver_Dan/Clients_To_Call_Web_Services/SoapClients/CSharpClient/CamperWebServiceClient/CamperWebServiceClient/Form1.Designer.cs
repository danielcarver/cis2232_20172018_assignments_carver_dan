﻿namespace CamperWebServiceClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCamperId = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtBxCamperId = new System.Windows.Forms.TextBox();
            this.btnGetName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCamperId
            // 
            this.lblCamperId.AutoSize = true;
            this.lblCamperId.Location = new System.Drawing.Point(35, 53);
            this.lblCamperId.Name = "lblCamperId";
            this.lblCamperId.Size = new System.Drawing.Size(55, 13);
            this.lblCamperId.TabIndex = 0;
            this.lblCamperId.Text = "Camper Id";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(38, 83);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(33, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "None";
            // 
            // txtBxCamperId
            // 
            this.txtBxCamperId.Location = new System.Drawing.Point(101, 53);
            this.txtBxCamperId.Name = "txtBxCamperId";
            this.txtBxCamperId.Size = new System.Drawing.Size(100, 20);
            this.txtBxCamperId.TabIndex = 2;
            // 
            // btnGetName
            // 
            this.btnGetName.Location = new System.Drawing.Point(123, 147);
            this.btnGetName.Name = "btnGetName";
            this.btnGetName.Size = new System.Drawing.Size(75, 23);
            this.btnGetName.TabIndex = 3;
            this.btnGetName.Text = "Get Name";
            this.btnGetName.UseVisualStyleBackColor = true;
            this.btnGetName.Click += new System.EventHandler(this.btnGetName_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnGetName);
            this.Controls.Add(this.txtBxCamperId);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblCamperId);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCamperId;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtBxCamperId;
        private System.Windows.Forms.Button btnGetName;
    }
}

