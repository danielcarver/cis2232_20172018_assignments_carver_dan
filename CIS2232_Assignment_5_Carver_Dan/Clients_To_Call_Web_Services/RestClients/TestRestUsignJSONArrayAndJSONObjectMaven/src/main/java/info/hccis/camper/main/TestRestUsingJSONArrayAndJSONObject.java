package info.hccis.camper.main;

import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestRestUsingJSONArrayAndJSONObject {

    final public static String MENU = "\nMain Menu \n"
            + "V) View Players\n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    private static final String URL_STRING = "http://localhost:8080/canes/rest/PlayerService/players";

    public static void main(String[] args) {
        boolean endProgram = false;
        do {
            System.out.println(MENU);
            String choice = input.nextLine();

            switch (choice.toUpperCase()) {
//                case "A":
//                    //NOTE that the post service has been implemented in the CamperServiceJpa (not CamperService)
//                    UtilityRest.addCamperUsingRest(createCamper());
//                    break;
                case "V":
                    String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);
                    //**************************************************************
                    //Based on the json string passed back, loop through each json
                    //object which is a json string in an array of json strings.
                    //*************************************************************
                    JSONArray jsonArray = new JSONArray(jsonReturned);
                    //**************************************************************
                    //For each json object in the array, show the first and last names
                    //**************************************************************
                    System.out.println("Here are the campers");
                    for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
                        JSONObject playerJson = jsonArray.getJSONObject(currentIndex);
                        System.out.println("Name: " + playerJson.getString("name") + " Amount Paid: $" + playerJson.getInt("amountPaid"));

                    }
                    break;

                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        } while (!endProgram);
    }

    /**
     * Create a camper object by passing asking user for input.
     * @return camper
     * @since 20171117
     * @author BJM
     */
    public static Camper createCamper() {
        Camper newCamper = new Camper();

        System.out.println("Enter first name:");
        newCamper.setFirstName(input.nextLine());

        System.out.println("Enter last name:");
        newCamper.setLastName(input.nextLine());

        System.out.println("Enter date of birth:");
        newCamper.setDob(input.nextLine());

        newCamper.setCampType(2);
        newCamper.setId(20);

        return newCamper;
    }

}
