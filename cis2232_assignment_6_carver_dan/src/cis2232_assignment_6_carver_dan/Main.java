package cis2232_assignment_6_carver_dan;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author dan carver
 * @since November 29th, 2017
 *
 */
public class Main {

    private static int group;
    private static String member;
    private static int hours;

    public static void main(String[] args) {
        Thread tennis1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner input = new Scanner(System.in);

                System.out.println("Welcome to the CIS Tennis Program");
                System.out.println("How many are in the group (1,2,3,4)");
                group = input.nextInt();
                input.nextLine();

                System.out.println("Are you a member (Y/N)?");
                member = input.nextLine();

                System.out.println("How many hours do you want for your lesson?");
                hours = input.nextInt();
                input.nextLine();

                Tennis tennis = new Tennis(group, member, hours);
                System.out.println(tennis.toString());

                writeToFile(tennis.toString() + "\r\n");
            }
        });

        Thread tennis2 = new Thread(new Runnable() {
            @Override
            public void run() {
                String group = JOptionPane.showInputDialog("Welcome to the CIS Tennis Program \r\n"
                        + "How many are in the group (1,2,3,4)");
                int groupSize = Integer.parseInt(group);

                String member = JOptionPane.showInputDialog("Are you a member (Y/N)?");

                String hour = JOptionPane.showInputDialog("How many hours do you want for your lesson?");
                int hours = Integer.parseInt(hour);

                Tennis tennis2 = new Tennis(groupSize, member, hours);

                JOptionPane.showMessageDialog(null, tennis2.toString());

                writeToFile(tennis2.toString() + "\r\n");
            }
        });

        tennis1.start();
        tennis2.run();
    }

    //taken from bj maclean
    public static synchronized void writeToFile(String textToWrite) {
        String FILE_NAME = "/cis2232/journal.txt";
        try {
            //Create a file
            Files.createDirectories(Paths.get("/cis2232"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }

        //Also write to the file when a new camper was added!!
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(FILE_NAME, true);
            bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
