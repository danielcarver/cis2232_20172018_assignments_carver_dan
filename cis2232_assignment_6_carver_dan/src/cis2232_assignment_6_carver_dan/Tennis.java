package cis2232_assignment_6_carver_dan;

import com.sun.xml.internal.bind.v2.schemagen.Util;

/**
 *
 * @author dan carver
 * @since November 29th, 2017
 * 
 */

public class Tennis {
    
    private int groupSize = 0;
    private String member = "";
    private int hours = 0;
    private int rate = 0;
    
    
    public Tennis ()
    {
        
    }
    
    public Tennis (int groupSize, String member, int hours)
    {
        this.groupSize = groupSize;
        this.member = member;
        this.hours = hours;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
    
    
    
    public int calculateCost ()
    {
        int cost = 0;
        
        switch (groupSize)
        {
            case 1:
                if (member.equalsIgnoreCase("y"))
                {
                    this.rate = 55;
                    cost = hours * rate;
                }
                
                if (member.equalsIgnoreCase("n"))
                {
                    this.rate = 60;
                    cost = hours * rate;
                }
                break;
            case 2:
                if (member.equalsIgnoreCase("y"))
                {
                    this.rate = 30;
                    cost = hours * rate;
                }
                
                if (member.equalsIgnoreCase("n"))
                {
                    this.rate = 33;
                    cost = hours * rate;
                }
                break;
            case 3:
                if (member.equalsIgnoreCase("y"))
                {
                    this.rate = 21;
                    cost = hours * rate;
                }
                
                if (member.equalsIgnoreCase("n"))
                {
                    this.rate = 23;
                    cost = hours * rate;
                }
                break;
            case 4:
                if (member.equalsIgnoreCase("y"))
                {
                    this.rate = 16;
                    cost = hours * rate;
                }
                
                if (member.equalsIgnoreCase("n"))
                {
                    this.rate = 18;
                    cost = hours * rate;
                }
                break;
        }
        
        return cost;
    }
    
    public String toString ()
    {
        int cost = 0;
        cost = calculateCost();
        return "Lesson details - Lesson group size: " + groupSize + " Member: " + member + " Rate: $" + this.rate
                +"/hour  Cost: $" + cost;
    }
}
