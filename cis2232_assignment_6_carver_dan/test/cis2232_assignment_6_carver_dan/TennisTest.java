package cis2232_assignment_6_carver_dan;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dan carver
 * @since November 29th, 2017
 * 
 */

public class TennisTest {
    
    public TennisTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void calculateCostPrivateMemberTest ()
    {
        Tennis result = new Tennis (1, "Y", 1);
        
        result.calculateCost();
        
        assertEquals(55, result.calculateCost());
    }
    
    @Test
    public void calculateCostPrivateNonMemberTest ()
    {
        Tennis result = new Tennis (1, "N", 2);
        
        result.calculateCost();
        
        assertEquals(120, result.calculateCost());   
    }
    
    @Test
    public void calculateCostGroupOfTwoMemberTest ()
    {
        Tennis result = new Tennis (2, "Y", 2);
        
        result.calculateCost();
        
        assertEquals(60, result.calculateCost());   
    }
    
    @Test
    public void calculateCostGroupOfTwoNonMemberTest ()
    {
        Tennis result = new Tennis (2, "n", 1);
        
        result.calculateCost();
        
        assertEquals(33, result.calculateCost());   
    }
    
    @Test
    public void calculateCostGroupOfThreeNonMemberTest ()
    {
        Tennis result = new Tennis (3, "n", 2);
        
        result.calculateCost();
        
        assertEquals(46, result.calculateCost());   
    }
    
    @Test
    public void calculateCostGroupOfFourMemberTest ()
    {
        Tennis result = new Tennis (4, "y", 4);
        
        result.calculateCost();
        
        assertEquals(64, result.calculateCost());   
    }
}
