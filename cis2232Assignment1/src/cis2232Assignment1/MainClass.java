package cis2232Assignment1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author dan carver
 * @since 20170925 
 *
 * handles logic for reading and writing players to the file.
 */
public class MainClass 
{

    public static String MENU = "Options:\nA) Add player\nS) Show players\nG) Update Amount\nX) Exit";
    public static String FILE_NAME = "/cis2232/players.txt";
    private static BufferedWriter bw = null;
    private static FileWriter fw = null;

    public static void main(String[] args) throws IOException 
    {
        //Create a file
        Files.createDirectories(Paths.get("/cis2232"));

        ArrayList<Player> theList = new ArrayList();
        loadPlayers(theList);
        String option;
        do 
        {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) 
            {
                case "A":
                    Player newPlayer = new Player(true);
                    theList.add(newPlayer);

                    try 
                    {
                        fw = new FileWriter(FILE_NAME, true);
                        bw = new BufferedWriter(fw);
                        bw.write(newPlayer.getCSV(true));
                        System.out.println("Done");
                    } 
                    catch (IOException e) 
                    {
                        e.printStackTrace();
                    } 
                    finally 
                    {
                        try 
                        {
                            if (bw != null) 
                            {
                                bw.close();
                            }

                            if (fw != null) 
                            {
                                fw.close();
                            }

                        } 
                        catch (IOException ex)
                        {

                            ex.printStackTrace();

                        }

                    }

                    break;
                case "S":
                    System.out.println("Here are the Players");
                    for (Player player : theList) 
                    {
                        System.out.println(player);
                    }
                    break;
                case "G":
                    // i know its ugly and should be in a method but i waited last minute to do this so i don't even give a heck
                    ArrayList<Player> newList = new ArrayList();

                    try 
                    {
                        System.out.println("Enter the ID of a player");

                        int regID = FileUtility.getInput().nextInt();
                        FileUtility.getInput().nextLine();

                        int totalAmounts = 0;

                        for (Player player : theList) 
                        {

                            if (player.getRegistrationId() == regID)
                            {
                                System.out.println("Enter the new amount");

                                int newAmount = FileUtility.getInput().nextInt();
                                FileUtility.getInput().nextLine();

                                Player tempPlayer = new Player();
                                tempPlayer = player;

                                tempPlayer.setAmountPaid(newAmount);

                                for (int i = 0; i < theList.size(); i++) 
                                {

                                    if (theList.get(i).equals(player)) 
                                    {
                                        theList.remove(i);
                                        theList.add(tempPlayer);

                                    }
                                }

                                for (int i = 0; i < theList.size(); i++) 
                                {
                                    newList.add(i, theList.get(i));
                                }

                            }

                            
                            int tempAmount = player.getAmountPaid();

                            totalAmounts += tempAmount;
                        }
                            
                            fw = new FileWriter(FILE_NAME);
                            bw = new BufferedWriter(fw);
                            for (int i = 0; i < newList.size(); i++)
                            {
                                bw.write(newList.get(i).getCSV(true));
                            }

                        System.out.println("The total amount paid by all players is $" + totalAmounts);

                        System.out.println("Done");
                    } 
                    catch (IOException e) 
                    {
                        e.printStackTrace();
                    } 
                    finally 
                    {
                        try 
                        {
                            if (bw != null) 
                            {
                                bw.close();
                            }

                            if (fw != null) 
                            {
                                fw.close();
                            }

                        } 
                        catch (IOException ex) 
                        {

                            ex.printStackTrace();

                        }
                    }

                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @author dan carver
     * @since 20170925
     */
    public static void loadPlayers(ArrayList players) 
    {
        System.out.println("Loading players from file");
        int count = 0;

        try 
        {
            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));

            for (String current : test) 
            {
                System.out.println("Loading:  " + current);
                //Get a player from the string
                Player temp = new Player(current);
                players.add(temp);
                count++;
            }

        } 
        catch (IOException ex) 
        {
            System.out.println("Error loading players from file.");
            System.out.println(ex.getMessage());
        }

        System.out.println("Finished...Loading players from file (Loaded " + count + " players)\n\n");

    }
}
