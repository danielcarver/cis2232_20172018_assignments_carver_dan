package cis2232Assignment1;

//import com.google.gson.Gson;
import java.util.Scanner;

/**
 * This class will represent a player.
 *
 * @author dan carver
 * @since 20170925
 */
public class Player 
{

    private static int maxRegistrationId;
    private int registrationId;
    private String name;
    private String parentName;
    private String phone;
    private String email;
    private int amountPaid;

    public Player() 
    {
        //do nothing.
    }

    /**
     * Default constructor which will get info from user
     *
     * @author dan carver
     * @since 20170925
     */
    public Player(boolean getFromUser) 
    {
        System.out.println("Enter player name");
        this.name = FileUtility.getInput().nextLine();

        System.out.println("Enter parent name");
        this.parentName = FileUtility.getInput().nextLine();

        System.out.println("Enter phone");
        this.phone = FileUtility.getInput().nextLine();
        
        System.out.println("Enter email");
        this.email = FileUtility.getInput().nextLine();
        
        System.out.println("Enter amount paid");
        this.amountPaid = FileUtility.getInput().nextInt();
        FileUtility.getInput().nextLine();

        this.registrationId = ++maxRegistrationId;
    }

    /**
     * Custom constructor with all info
     *
     * @author dan carver
     * @since 20170925
     */
    public Player(int registrationId, String name, String parentName, String phone, String email, int amountPaid) 
    {
        this.registrationId = registrationId;
        this.name = name;
        this.parentName = parentName;
        this.phone = phone;
        this.email = email;
        this.amountPaid = amountPaid;
    }


    /**
     * constructor which will create from String array
     *
     * @author dan carver
     * @since 20170925
     */
    public Player(String[] parts) 
    {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3], parts[4], Integer.parseInt(parts[5]));
        
        //keeping track of the reg id for when new players are created
        if (Integer.parseInt(parts[0]) > maxRegistrationId) 
        {
            maxRegistrationId = Integer.parseInt(parts[0]);
        }
    }

    /**
     * constructor which will create from String array
     *
     * @author dan carver
     * @since 20170925
     */
    public Player(String csvValues) 
    {
        this(csvValues.split(","));
    }

    
    public String getCSV() 
    {
        return registrationId + "," + name + "," + parentName + "," + phone + ","
                + email + "," + amountPaid;
    }

    /**
     * This will give back the Player details
     * 
     * @author dan carver
     * @since 20170925
     */
    public String getCSV(boolean withLineFeed)
    {
        if(withLineFeed)
        {
            return getCSV()+System.lineSeparator();
        }
        else
        {
            return getCSV();
        }
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(int amountPaid) {
        this.amountPaid = amountPaid;
    }

    @Override
    public String toString() 
    {
        return "registrationId " + registrationId + ", name " + name + ", parentName " + parentName + ", phone " + phone + ", email " + email + ", amountPaid " + amountPaid;
    }

}
