package info.hccis.player.model.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author BJ
 */
@Entity
@Table(name = "CodeValue")
public class CodeValue implements Serializable {

    private int codeTypeId;

    @Id
    private int codeValueSequence;

    @NotEmpty(message = "Required")
    @Size(min = 1, max = 100, message = "Size between 1-100 chars")
    private String englishDescription;

    @NotEmpty(message = "Required")
    private String englishDescriptionShort;

    @Transient
    private int[] testInt = {100, 200, 300};

    @Transient
    private ArrayList<String> testIntArrayList = new ArrayList();

    public CodeValue() {
        //nothing.
        this.testIntArrayList.add("A");
        this.testIntArrayList.add("B");
        this.testIntArrayList.add("C");

    }

    public CodeValue(int codeTypeId, int codeValueSequence, String description, String descriptionShort) {
        this.codeTypeId = codeTypeId;
        this.codeValueSequence = codeValueSequence;
        this.englishDescription = description;
        this.englishDescriptionShort = descriptionShort;
    }

    public int getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(int codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    public int getCodeValueSequence() {
        return codeValueSequence;
    }

    public void setCodeValueSequence(int codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescriptionShort() {
        return englishDescriptionShort;
    }

    public void setEnglishDescriptionShort(String englishDescriptionShort) {
        this.englishDescriptionShort = englishDescriptionShort;
    }


    public int[] getTestInt() {
        return testInt;
    }

    public void setTestInt(int[] testInt) {
        this.testInt = testInt;
    }

    public ArrayList<String> getTestIntArrayList() {
        return testIntArrayList;
    }

    public void setTestIntArrayList(ArrayList testIntArrayList) {
        this.testIntArrayList = testIntArrayList;
    }

}
