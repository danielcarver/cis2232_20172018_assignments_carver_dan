package info.hccis.player.web;

import info.hccis.player.bo.PlayerValidationBO;
import info.hccis.player.model.*;
import info.hccis.player.dao.PlayerDAO;
import info.hccis.player.model.Player;
import info.hccis.player.model.jpa.User;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PlayerController {
    
    @RequestMapping("/player/add")
    public String add(Model model, HttpSession session) {

        //make sure logged in
//        User user = (User) session.getAttribute("loggedInUser");
//        if (user == null) {
//            model.addAttribute("user", new User());
//            return "other/welcome";
//        }

        Player player = new Player();
        model.addAttribute("player", player);

        return "player/add";
    }

    @RequestMapping("/player/delete")
    public String delete(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        String name = request.getParameter("name");
        PlayerDAO.delete(name);

        ArrayList<Player> players = PlayerDAO.selectAll();
        model.addAttribute("players", players);
        return "player/list";
    }

    @RequestMapping("/player/edit")
    public String edit(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        String name = request.getParameter("name");
        Player player = PlayerDAO.select(name);
        model.addAttribute("player", player);

        return "player/edit";
    }

    @RequestMapping("/player/addSubmit")
    public String addSubmit(Model model, @ModelAttribute("player") Player player, HttpSession session) {

        //make sure logged in
//        User user = (User) session.getAttribute("loggedInUser");
//        if (user == null) {
//            model.addAttribute("user", new User());
//            return "other/welcome";
//        }

        ArrayList<String> errors = PlayerValidationBO.validatePlayer(player);
        //If there is an error send them back to add page.

        boolean error = false;
        if (!errors.isEmpty()) {
            error = true;
        }

        if (error) {
            model.addAttribute("messages", errors);
            return "/player/add";
        }

        System.out.println("BJM - About to add " + player + " to the database");
        try {
            PlayerDAO.insert(player);
            //Get the players from the database
            ArrayList<Player> players = PlayerDAO.selectAll();
            model.addAttribute("players", players);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding player to the database");
        }
        return "player/list";
    }
    
    @RequestMapping("/player/editSubmit")
    public String editSubmit(Model model, @ModelAttribute("player") Player player, HttpSession session) {

        //make sure logged in
//        User user = (User) session.getAttribute("loggedInUser");
//        if (user == null) {
//            model.addAttribute("user", new User());
//            return "other/welcome";
//        }

        ArrayList<String> errors = PlayerValidationBO.validatePlayer(player);
        //If there is an error send them back to add page.

        boolean error = false;
        if (!errors.isEmpty()) {
            error = true;
        }

        if (error) {
            model.addAttribute("messages", errors);
            return "/player/list";
        }

        System.out.println("BJM - About to add " + player + " to the database");
        try {
            PlayerDAO.update(player);
            //Get the players from the database
            ArrayList<Player> players = PlayerDAO.selectAll();
            model.addAttribute("players", players);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding player to the database");
        }
        return "player/list";
    }

    @RequestMapping("/player/list")
    public String showHome(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/welcome";
        }

        //Get the players from the database
        ArrayList<Player> players = PlayerDAO.selectAll();
        System.out.println("BJM-found " + players.size() + " players.  Going to welcome page");
        model.addAttribute("players", players);

        //This will send the user to the welcome.html page.
        return "player/list";
    }

}
