//package info.hccis.player.dao;
//
//import info.hccis.player.dao.util.ConnectionUtils;
//import info.hccis.player.dao.util.DbUtils;
//import info.hccis.player.model.DatabaseConnection;
//import info.hccis.player.model.jpa.User;
//import info.hccis.player.util.Utility;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.ArrayList;
//import java.util.ResourceBundle;
//
//public class UserDAO {
//
//    public static String getUserTypeCode(String database, String username, String password) {
//        Connection conn = null;
//        PreparedStatement ps = null;
//        String sql;
//
//        try {
//            //connect to database
//            String propFileName = "spring.data-access";
//            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
//            String dbUserName = rb.getString("jdbc.username");
//            String dbPassword = rb.getString("jdbc.password");
//            System.out.println("BJM-Set the database to "+database);
//
//            DatabaseConnection newConnection = new DatabaseConnection(database, dbUserName, dbPassword);
//            conn = ConnectionUtils.getConnection(newConnection);
//
//            //create query to get usertypecode
//            sql = "SELECT `userTypeCode` FROM `UserAccess` WHERE `username` = '" + username + "' AND `password`= '" + Utility.getHashPassword(password) + "'";
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//
//            //get data if statement returns a value
//            String userTypeCode = null;
//            while (rs.next()) {
//                userTypeCode = Integer.toString(rs.getInt("userTypeCode"));
//            }
//
//            //return if data found
//            if (userTypeCode != null) {
//                return userTypeCode;
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            System.out.println("ERROR GETTING: User Type Code"
//                    + "WHY: " + errorMessage);
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        //if data isn't found return 0
//        return "0";
//    }
//
//    public static ArrayList<User> getUsers(DatabaseConnection databaseConnection) {
//        ArrayList<User> users = new ArrayList();
//
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        try {
//            conn = ConnectionUtils.getConnection(databaseConnection);
//
//            sql = "SELECT * FROM `User` order by userId";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                User user = new User();
//                user.setUserId(rs.getInt("userId"));
//                user.setUsername(rs.getString("username"));
//                user.setLastName(rs.getString("lastName"));
//                user.setFirstName(rs.getString("firstName"));
//                user.setUserTypeCode(rs.getInt("userTypeCode"));
//                user.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(databaseConnection, 1, user.getUserTypeCode()));
//                user.setCreatedDateTime(rs.getString("createdDateTime"));
//                users.add(user);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//
//        return users;
//    }
//
//    public static synchronized String insert(DatabaseConnection databaseConnection, User user) throws Exception {
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        String results = "";
//
//        try {
//            conn = ConnectionUtils.getConnection(databaseConnection);
//            String now = Utility.getNow("yyyy-MM-dd HH:mm:ss");
//
//            sql = "INSERT INTO `User`(`userId`, `username`, `password`, lastName, firstName, additional1, additional2, `userTypeCode`, `createdDateTime`)"
//                    + "VALUES (?,?,?,?,?,?,?,?,'"+now+"')";
//
//            if(user.getUserId() == null){
//                user.setUserId(0);
//            }
//            
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, user.getUserId());
//            ps.setString(2, user.getUsername());
//            ps.setString(3, user.getPassword());
//            ps.setString(4, user.getLastName());
//            ps.setString(5, user.getFirstName());
//            ps.setString(6, "");
//            ps.setString(7, "");
//            ps.setInt(8, user.getUserTypeCode());
//            ps.execute();
//            results = "success";
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            results = "Fail";
//            throw e;
//
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return results;
//
//    }
//
//    public static synchronized String update(DatabaseConnection databaseConnection, User user) throws Exception {
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        try {
//            conn = ConnectionUtils.getConnection(databaseConnection);
//
//            sql = "UPDATE `User` SET `username` = '" + user.getUsername() + "', `password` = '" + user.getPassword() 
//                    + "', `lastName` = " + user.getLastName()+ "', `firstName` = " + user.getFirstName()
//                    + "', additional1 = " + user.getAdditional1() + "', additional2 = " + user.getAdditional2()
//                    + "', `userTypeCode` = " + user.getUserTypeCode() + " WHERE userId = " + user.getUserId();
//            System.out.println(sql);
//            ps = conn.prepareStatement(sql);
//            ps.execute();
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//
//        return "Test";
//    }
//
//    public static synchronized String delete(DatabaseConnection databaseConnection, int userId) throws Exception {
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        try {
//            conn = ConnectionUtils.getConnection(databaseConnection);
//
//            sql = "DELETE FROM User WHERE userId = " + userId;
//
//            ps = conn.prepareStatement(sql);
//            ps.execute();
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//
//        return "Test";
//    }
//
//    public static synchronized User selectUser(DatabaseConnection databaseConnection, int userId) throws Exception {
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        ResultSet rs = null;
//        User userSelect = new User();
//        System.out.println(userId);
//
//        try {
//            conn = ConnectionUtils.getConnection(databaseConnection);
//
//            sql = "SELECT * FROM `UserAccess` WHERE userAccessId = " + userId;
//            System.out.println(sql);
//            ps = conn.prepareStatement(sql);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                userSelect.setUserId(rs.getInt("userId"));
//                userSelect.setUsername(rs.getString("username"));
//                userSelect.setPassword(rs.getString("password"));
//                userSelect.setLastName(rs.getString("lastName"));
//                userSelect.setFirstName(rs.getString("firstName"));
//                userSelect.setAdditional1(rs.getString("additional1"));
//                userSelect.setAdditional2(rs.getString("additional2"));
//                userSelect.setUserTypeCode(rs.getInt("userTypeCode"));
//                userSelect.setCreatedDateTime(rs.getString("createdDateTime"));
//            }
//
//            System.out.println(userSelect.getUsername());
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//
//        return userSelect;
//    }
//}
