package info.hccis.src;

import info.hccis.src.FileUtility;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import info.hccis.dao.PlayerDAO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dan carver
 * @since 20170925 
 *
 * handles logic for reading and writing players to database.
 */
public class Main
{

    public static String MENU = "Options:\nA) Add player\nS) Show players\nG) Update Amount\nX) Exit";

    public static void main(String[] args) throws IOException 
    {
        
        String option;
        do 
        {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) 
            {
                case "A":
                    Player newPlayer = new Player(true);

                    try 
                    {
                        PlayerDAO.insert(newPlayer);
                    } 
                    catch (Exception e) 
                    {
                        e.printStackTrace();
                    }

                    break;
                case "S":
                    System.out.println("Here are the Players");
                    
                    for (Player player : PlayerDAO.selectAll())
                    {
                        System.out.println(player);
                    }
                    break;
                case "G":
                    // i know its ugly and should be in a method but i waited last minute to do this so i don't even give a heck
                    
                    try
                    {
                        
                    System.out.println("Enter the name of a player");

                        String name = FileUtility.getInput().nextLine();

                        int totalAmounts = 0;
                        
                        Player player = null;
                        
                        player = PlayerDAO.select(name);
                        player.edit();
                        
                        
                        try 
                        {
                        PlayerDAO.update(player);
                        PlayerDAO.total();
                        } 
                        catch (Exception ex) 
                        {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                            
                    } 
                    catch (Exception e) 
                    {
                        e.printStackTrace();
                    }

                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @author dan carver
     * @since 20170925
     */
//    public static void loadPlayers(ArrayList players) 
//    {
//        System.out.println("Loading players from file");
//        int count = 0;
//
//        try 
//        {
//            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));
//
//            for (String current : test) 
//            {
//                System.out.println("Loading:  " + current);
//                //Get a player from the string
//                Player temp = new Player(current);
//                players.add(temp);
//                count++;
//            }
//
//        } 
//        catch (IOException ex) 
//        {
//            System.out.println("Error loading players from file.");
//            System.out.println(ex.getMessage());
//        }
//
//        System.out.println("Finished...Loading players from file (Loaded " + count + " players)\n\n");
//
//    }
}
